import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FeaturedService} from '../services/featured.service';
import {Vacancy} from '../../models/vacancy.model';
import {HttpErrorResponse} from '@angular/common/http';
import {NotificationType} from '../../enum/notification-type.enum';
import {NotificationService} from '../../shared-service/notification.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-featured',
  templateUrl: './featured.component.html',
  styleUrls: ['./featured.component.css']
})
export class FeaturedComponent implements OnInit, OnDestroy{

  vacancies: Vacancy[] = [];
  private subscription: Subscription[] = [];

  @ViewChild('makeBidButton') makeBidButton: ElementRef | undefined
  @ViewChild('already') already: ElementRef | undefined
  @ViewChild('deleteButton') deleteButton: ElementRef | undefined
  @ViewChild('deletedAlready') deletedAlready: ElementRef | undefined

    constructor(private featuredService: FeaturedService,
                private notificationService: NotificationService
    ) { }

  ngOnInit(): void {
    this.subscription.push(
    this.featuredService.getAllFeaturedVacancies()
        .subscribe((response: Vacancy[]) => {
           this.vacancies = response;
        }, (errorResponse: HttpErrorResponse) => {
            this.notificationService.notify(NotificationType.ERROR, "An error occurred, please try later");
        }));
  }

  makeBid(id: number):void {
      this.subscription.push(
      this.featuredService.makeBid(id)
          .subscribe( () => {
            this.makeBidButton?.nativeElement.style.display.none;
            this.already?.nativeElement.style.display.block;
            this.deleteButton?.nativeElement.style.display.none;
          }, (errorResponse: HttpErrorResponse) => {
              this.notificationService.notify(NotificationType.ERROR, "An error occurred, please try later");
          }));
  }

  deleteFromList(id: number) {
      this.subscription.push(
      this.featuredService.deleteFromFeatured(id)
          .subscribe( () => {
              this.makeBidButton?.nativeElement.style.display.none;
              this.deleteButton?.nativeElement.style.display.none;
              this.deletedAlready?.nativeElement.style.display.block;
          }, (errorResponse: HttpErrorResponse) => {
              this.notificationService.notify(NotificationType.ERROR, "An error occurred, please try later");
          }));
  }

  ngOnDestroy(): void {
      this.subscription.forEach(sub => sub.unsubscribe());
  }
}
