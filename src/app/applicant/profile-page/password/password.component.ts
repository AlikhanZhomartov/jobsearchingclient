import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MyValidators} from '../../my.validators';
import {PasswordService} from '../../services/password.service';
import {Subscription} from 'rxjs';
import {NotificationService} from '../../../shared-service/notification.service';
import {NotificationType} from '../../../enum/notification-type.enum';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.css']
})
export class PasswordComponent implements OnInit, OnDestroy {

  public form: FormGroup;
  private subscription: Subscription[] = [];
  public loaderButton: boolean = false;

  constructor(private passwordService: PasswordService,
              private notificationService: NotificationService
  ) {
    this.form = this.createForm();
  }

  ngOnInit(): void {
  }

  private createForm(): FormGroup {
    return this.form = new FormGroup({
      oldPassword: new FormControl('', [Validators.required, Validators.minLength(6)]),
      newPassword: new FormControl('', [Validators.required, Validators.minLength(6)]),
      repeatedPassword: new FormControl('', [Validators.required, Validators.minLength(6)])
    });
  }


  submit() {
    if (!this.form.invalid && this.form.get('newPassword')?.value == this.form.get('repeatedPassword')?.value) {
      this.loaderButton = !this.loaderButton;
      this.subscription.push(
      this.passwordService.saveNewPassword(this.form.value.oldPassword, this.form.value.newPassword)
          .subscribe(() => {
            this.loaderButton = !this.loaderButton;
            this.form.reset();
            this.notificationService.notify(NotificationType.SUCCESS, "Your password has been changed successfully!");
          }, (errorResponse: HttpErrorResponse) => {
            this.loaderButton = !this.loaderButton;
            this.form.reset();
            this.notificationService.notify(NotificationType.ERROR, "An error occurred, please try later!");
          }));
    }
  }

  ngOnDestroy(): void {
    this.subscription.forEach(sub => sub.unsubscribe());
  }

}
