import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {NameSurnameService} from '../../services/name-surname.service';
import {User} from '../../../models/user.models';
import {Subscription} from 'rxjs';
import {NotificationService} from '../../../shared-service/notification.service';
import {HttpErrorResponse} from '@angular/common/http';
import {NotificationType} from '../../../enum/notification-type.enum';

@Component({
  selector: 'app-name-surname',
  templateUrl: './name-surname.component.html',
  styleUrls: ['./name-surname.component.css']
})
export class NameSurnameComponent implements OnInit, OnDestroy{

  public form: FormGroup;
  private subscription: Subscription[] = [];
  public loaderButton: boolean = false;

  constructor(private nameAndSurnameService: NameSurnameService,
              private notificationService: NotificationService
  ) {
    this.form = this.createForm();
  }

  ngOnInit(): void {
   this.subscription.push(
   this.nameAndSurnameService.getNameAndSurname()
       .subscribe( (response: User) => {
         if (response != null) {
           this.form.get('name')?.setValue(response.firstname);
           this.form.get('surname')?.setValue(response.lastname);
         }
       }, (errorResponse: HttpErrorResponse) => {
           this.notificationService.notify(NotificationType.ERROR, "An error occurred, please try later!");
       }));
  }

  private createForm(): FormGroup {
    return this.form = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z ]*[А-Яа-яЁё ]*$')]),
      surname: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z ]*[А-Яа-яЁё ]*$')])
    });
  }

  submit() {
    if (!this.form.invalid) {
      this.loaderButton = !this.loaderButton;
      this.subscription.push(
      this.nameAndSurnameService.saveNameAndSurname(this.form.value.name, this.form.value.surname)
          .subscribe( () => {
              this.loaderButton = !this.loaderButton;
              this.notificationService.notify(NotificationType.SUCCESS, "Name and surname have been saved!");
          }, (errorResponse: HttpErrorResponse) => {
              this.loaderButton = !this.loaderButton;
              this.notificationService.notify(NotificationType.ERROR, "An error occurred, please try later!");
          }));
    }
  }

  ngOnDestroy(): void {
      this.subscription.forEach(sub => sub.unsubscribe());
  }
}
