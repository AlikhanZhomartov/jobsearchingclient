import {Component, OnInit} from '@angular/core';
import { ResizedEvent } from 'angular-resize-event';
import {TokenStorageService} from '../../../../shared-service/token-storage.service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-applicant-layout',
  templateUrl: './applicant-layout.component.html',
  styleUrls: ['./applicant-layout.component.css']
})
export class ApplicantLayoutComponent implements OnInit {

  isShown: boolean;

  constructor(private tokenService: TokenStorageService,
              private router: Router) {
    this.isShown = false;
  }

  ngOnInit(): void {

  }

  toggleShow():void {
    this.isShown = !this.isShown;
  }

  logout(): void {
    this.tokenService.logout();
    this.router.navigate(['/']);
  }

  onResized(event: ResizedEvent){
    if (event.newWidth >= 750) {
      this.isShown = !this.isShown;
    }
  }

}
