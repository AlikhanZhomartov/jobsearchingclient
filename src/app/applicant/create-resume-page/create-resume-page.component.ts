import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MyValidators} from '../my.validators';
import {Resume} from '../../models/resume.model';
import {CreateResumeService} from '../services/create-resume.service';
import {Subscription} from 'rxjs';
import {HttpErrorResponse} from '@angular/common/http';
import {NotificationService} from '../../shared-service/notification.service';
import {NotificationType} from '../../enum/notification-type.enum';

@Component({
  selector: 'app-create-resume-page',
  templateUrl: './create-resume-page.component.html',
  styleUrls: ['./create-resume-page.component.css']
})
export class CreateResumePageComponent implements OnInit, OnDestroy {

  public form: FormGroup;
  public educationTypeHide: boolean = true;
  private resume: Resume | undefined;
  private subscription: Subscription[] = [];
  public loadButton: boolean = false;

  constructor(private createResumeService: CreateResumeService,
              private notificationService: NotificationService
  ) {
    this.form = this.createForm();
  }

  ngOnInit(): void {
    this.subscription.push(
     this.createResumeService.getResume()
         .subscribe((response: Resume) => {
          this.resume = response;
          if (this.resume != null) {
            this.form.get('name')?.setValue(this.resume.name);
            this.form.get('surname')?.setValue(this.resume.surname);
            this.form.get('mobile')?.setValue(this.resume.mobile);
            this.form.get('city')?.setValue(this.resume.city);
            this.form.get('day')?.setValue(this.resume.day);
            this.form.get('month')?.setValue(this.resume.month);
            this.form.get('year')?.setValue(this.resume.year);
            this.form.get('gender')?.setValue(this.resume.gender);
            this.form.get('jobTitle')?.setValue(this.resume.jobTitle);
            this.form.get('salary')?.setValue(this.resume.salary);
            this.form.get('currency')?.setValue(this.resume.currency);
            this.form.get('aboutMe')?.setValue(this.resume.aboutMe);
            this.form.get('educationType')?.setValue(this.resume.educationType);
            this.form.get('institution')?.setValue(this.resume.institution);
            this.form.get('faculty')?.setValue(this.resume.faculty);
            this.form.get('specialization')?.setValue(this.resume.specialization);
            this.form.get('graduateYear')?.setValue(this.resume.graduateYear);
          }
         }, (errorResponse: HttpErrorResponse) => {
           this.notificationService.notify(NotificationType.ERROR, "An error occurred, please try later!");
         }));
  }

  private createForm(): FormGroup {
    return this.form = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z ]*[А-Яа-яЁё ]*$')]),
      surname: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z ]*[А-Яа-яЁё ]*$')]),
      mobile: new FormControl('+7', [Validators.required, Validators.pattern('[\\+]\\d{11}')]),
      city: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z ]*[А-Яа-яЁё ]*$')]),
      day: new FormControl('', [Validators.required, Validators.pattern('^(0[1-9]|[12]\\d|3[01])*$')]),
      month: new FormControl('', Validators.required),
      year: new FormControl('', [Validators.required, Validators.maxLength(4), Validators.minLength(4), MyValidators.yearRange]),
      gender: new FormControl('', Validators.required),
      jobTitle: new FormControl(''),
      salary: new FormControl(''),
      currency: new FormControl('KZT'),
      aboutMe: new FormControl('', [Validators.required, Validators.minLength(50)]),
      educationType: new FormControl('Среднее'),
      institution: new FormControl(''),
      faculty: new FormControl(''),
      specialization: new FormControl(''),
      graduateYear: new FormControl('', [Validators.minLength(4), Validators.maxLength(4)])
    });
  }

  educationType(){
    if(this.form.get('educationType')?.value == "Среднее") {
      this.educationTypeHide = true;
      this.form.get('institution')?.clearValidators();
      this.form.get('faculty')?.clearValidators();
      this.form.get('specialization')?.clearValidators();
      this.form.get('graduateYear')?.clearValidators();

      this.form.get('institution')?.updateValueAndValidity();
      this.form.get('faculty')?.updateValueAndValidity();
      this.form.get('specialization')?.updateValueAndValidity();
      this.form.get('graduateYear')?.updateValueAndValidity();

    }
    else {
      this.educationTypeHide = false;
      this.form.get('institution')?.setValidators(Validators.required);
      this.form.get('faculty')?.setValidators(Validators.required);
      this.form.get('specialization')?.setValidators(Validators.required);
      this.form.get('graduateYear')?.setValidators( Validators.required);

      this.form.get('institution')?.updateValueAndValidity();
      this.form.get('faculty')?.updateValueAndValidity();
      this.form.get('specialization')?.updateValueAndValidity();
      this.form.get('graduateYear')?.updateValueAndValidity();
    }
  }

  submit() {
    if (!this.form.invalid) {

      const resume: Resume = {
        name:  this.form.value.name,
        surname: this.form.value.name,
        mobile: this.form.value.mobile,
        city: this.form.value.city,
        day: this.form.value.day,
        month: this.form.value.month,
        year: this.form.value.year,
        gender: this.form.value.gender,
        jobTitle: this.form.value.jobTitle,
        salary: this.form.value.salary,
        currency: this.form.value.currency,
        aboutMe: this.form.value.aboutMe,
        educationType: this.form.value.educationType,
        institution: this.form.value.institution,
        faculty: this.form.value.faculty,
        specialization: this.form.value.specialization,
        graduateYear: this.form.value.graduateYear,
      }
      this.loadButton = !this.loadButton;
      this.subscription.push(
      this.createResumeService.saveResume(resume)
          .subscribe( () => {
            this.loadButton = !this.loadButton;
            this.notificationService.notify(NotificationType.SUCCESS, "Your resume has been saved!");
          }, (errorResponse: HttpErrorResponse) => {
            this.loadButton = !this.loadButton;
            this.notificationService.notify(NotificationType.ERROR, "An error occurred, please try later!");
          }));
    }
  }

  ngOnDestroy(): void {
    this.subscription.forEach(sub => sub.unsubscribe());
  }

}
