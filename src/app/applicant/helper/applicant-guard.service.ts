import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {TokenStorageService} from '../../shared-service/token-storage.service';
import {Observable} from 'rxjs';
import {NotificationService} from '../../shared-service/notification.service';
import {NotificationType} from '../../enum/notification-type.enum';
import {User} from '../../models/user.models';

@Injectable({
  providedIn: 'root'
})
export class ApplicantGuardService implements CanActivate{

  constructor(private router: Router,
              private tokenService: TokenStorageService,
              private notification: NotificationService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    const currentUser: User = this.tokenService.getUser();
    if (currentUser != null && currentUser.role == "ROLE_USER") {
      return true;
    }else {
      this.router.navigate(['/']);
      this.notification.notify(NotificationType.ERROR, "You do not have an authority to access this page");
      return false;
    }
  }

}
