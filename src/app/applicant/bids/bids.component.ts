import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Bids} from '../../models/bids.model';
import {Vacancy} from '../../models/vacancy.model';
import {BidsService} from '../services/bids.service';
import {Subscription} from 'rxjs';
import {HttpErrorResponse} from '@angular/common/http';
import {NotificationType} from '../../enum/notification-type.enum';
import {NotificationService} from '../../shared-service/notification.service';

@Component({
  selector: 'app-bids',
  templateUrl: './bids.component.html',
  styleUrls: ['./bids.component.css']
})
export class BidsComponent implements OnInit, OnDestroy {

  bids: Bids[] = [];
  recommendedVacancies: Vacancy[] = [];
  private subscription: Subscription[] = [];

  @ViewChild('deletedBidButton') deletedBidButton: ElementRef | undefined;
  @ViewChild('deleteBidAlready') deleteBidAlready: ElementRef | undefined;
  @ViewChild('makeBidButton') makeBidButton: ElementRef | undefined;
  @ViewChild('makeBidAlready') makeBidAlready: ElementRef | undefined;
  @ViewChild('saveButton') saveButton: ElementRef | undefined;
  @ViewChild('saveAlready') saveAlready: ElementRef | undefined;

  constructor(private bidsService: BidsService,
              private notificationService: NotificationService
  ) { }

  ngOnInit(): void {
    this.subscription.push(
    this.bidsService.getAllBids()
        .subscribe((response: Bids[]) =>{
          this.bids = response;
        }, (errorResponse: HttpErrorResponse) => {
            this.notificationService.notify(NotificationType.ERROR, "An error occurred, please try later");
        }))
    this.subscription.push(
    this.bidsService.getAllRecommendations()
        .subscribe((response: Vacancy[]) => {
          this.recommendedVacancies = response;
        }, (errorResponse: HttpErrorResponse) => {
            this.notificationService.notify(NotificationType.ERROR, "An error occurred, please try later");
        })
    )
  }

  deleteBid(id: number) {
      this.subscription.push(
      this.bidsService.deleteBid(id)
          .subscribe( () => {
            this.deletedBidButton?.nativeElement.style.display.none;
            this.deleteBidAlready?.nativeElement.style.display.block;
          }, (errorResponse: HttpErrorResponse) => {
              this.notificationService.notify(NotificationType.ERROR, "An error occurred, please try later");
          }))
  }

  makeBid(id: number) {
    this.subscription.push(
    this.bidsService.makeBid(id)
        .subscribe( () => {
          this.makeBidButton?.nativeElement.style.display.none;
          this.makeBidAlready?.nativeElement.style.display.block;
          this.saveButton?.nativeElement.style.display.none;
        }, (errorResponse: HttpErrorResponse) => {
            this.notificationService.notify(NotificationType.ERROR, "An error occurred, please try later");
        }))
  }

  makeFeatured(id: number) {
    this.subscription.push(
    this.bidsService.saveInFeatured(id)
        .subscribe( () => {
          this.makeBidButton?.nativeElement.style.display.none;
          this.saveAlready?.nativeElement.style.display.block;
          this.saveButton?.nativeElement.style.display.none;
        }, (errorResponse: HttpErrorResponse) => {
            this.notificationService.notify(NotificationType.ERROR, "An error occurred, please try later");
        }))
  }

  ngOnDestroy(): void {
      this.subscription.forEach(sub => sub.unsubscribe());
  }
}
