import {FormControl, FormGroup} from '@angular/forms';

export class MyValidators {

    static yearRange(control: FormControl): {[key: string]: boolean} {
        const year:number = control.value;
        if (year >= new Date().getFullYear() - 120 && year <= new Date().getFullYear() - 14) {
            return {};
        }else{
            return {yearRange: true};
        }

    }

}
