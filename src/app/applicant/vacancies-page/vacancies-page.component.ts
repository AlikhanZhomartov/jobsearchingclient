import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Vacancy} from '../../models/vacancy.model';
import {VacanciesService} from '../services/vacancies.service';
import {HttpErrorResponse} from '@angular/common/http';
import {NotificationService} from '../../shared-service/notification.service';
import {NotificationType} from '../../enum/notification-type.enum';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-vacancies-page',
  templateUrl: './vacancies-page.component.html',
  styleUrls: ['./vacancies-page.component.css']
})

export class VacanciesPageComponent implements OnInit, OnDestroy {

  private subscription: Subscription[] = [];

  salary: string = "any";
  experience: string = "any";
  busyness: string = "any";
  accommodation: string = "any";
  selectedFilter: string = "any";

  vacancies: Vacancy[] = [];

  @ViewChild('makeBidButton') makeBidButton: ElementRef | undefined;
  @ViewChild('makeBidAlready') makeBidAlready: ElementRef | undefined;
  @ViewChild('saveButton') saveButton: ElementRef | undefined;
  @ViewChild('saveAlready') saveAlready: ElementRef | undefined;

  constructor(private vacancyService: VacanciesService,
              private notificationService: NotificationService
  ) {}

  ngOnInit(): void {
   this.subscription.push(
   this.vacancyService.getAllVacancies()
       .subscribe((response: Vacancy[]) => {
         this.vacancies = response
       }, (errorResponse: HttpErrorResponse) => {
           this.notificationService.notify(NotificationType.ERROR, "An error occurred, please try later");
       }));
  }

  discharge() {
    this.salary = "any";
    this.experience = "any";
    this.busyness = "any";
    this.accommodation = "any"
  }

  makeBid(id: number) {
    this.subscription.push(
    this.vacancyService.makeBids(id)
        .subscribe(() => {
          this.makeBidButton?.nativeElement.style.display.none;
          this.makeBidAlready?.nativeElement.style.display.block;
          this.saveButton?.nativeElement.style.display.none;
        }, (errorResponse: HttpErrorResponse) => {
            this.notificationService.notify(NotificationType.ERROR, "An error occurred, please try again");
        }))
  }

  saveVacancy(id: number) {
    this.subscription.push(
    this.vacancyService.saveVacancy(id)
        .subscribe( () => {
          this.makeBidButton?.nativeElement.style.display.none;
          this.saveAlready?.nativeElement.style.display.block;
          this.saveButton?.nativeElement.style.display.none;
        }, (errorResponse: HttpErrorResponse) => {
            this.notificationService.notify(NotificationType.ERROR, "An error occurred, please try again");
        }))
  }

  ngOnDestroy(): void {
      this.subscription.forEach(sub => sub.unsubscribe());
  }
}
