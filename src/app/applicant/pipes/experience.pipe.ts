import { Pipe, PipeTransform } from '@angular/core';
import {Vacancy} from '../../models/vacancy.model';


@Pipe({
  name: 'experience'
})
export class ExperiencePipe implements PipeTransform {

  transform(vacancies: Vacancy[], filter:string): Vacancy[] {

    if (filter == "any") {
      return vacancies;
    } else if (filter == "have") {
      return vacancies.filter(vacancies => vacancies.experience)
    }
    return vacancies.filter(vacancies => !vacancies.experience)

  }

}
