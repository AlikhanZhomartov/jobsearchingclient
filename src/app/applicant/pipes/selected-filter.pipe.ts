import { Pipe, PipeTransform } from '@angular/core';
import {Vacancy} from '../../models/vacancy.model';


@Pipe({
  name: 'selectedFilter'
})
export class SelectedFilterPipe implements PipeTransform {

  transform(vacancies: Vacancy[], filter:string): Vacancy[] {

    if (filter == "any") {
      return vacancies;
    } else if (filter == "dropping salary") {
      return vacancies.filter(vacancies => vacancies.salary != null).sort((a, b) =>  <number>b.salary -  <number>a.salary)
    }
    return vacancies.filter(vacancies => vacancies.salary != null).sort((a, b) => <number>a.salary -  <number>b.salary)

  }

}
