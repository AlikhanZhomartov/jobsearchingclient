import { Pipe, PipeTransform } from '@angular/core';
import {Vacancy} from '../../models/vacancy.model';

@Pipe({
  name: 'accommodation'
})
export class AccommodationPipe implements PipeTransform {

  transform(vacancies: Vacancy[], filter:string): Vacancy[] {

    if (filter == "any") {
      return vacancies;
    }
    else if (filter == "office"){
      return vacancies.filter(vacancies => vacancies.accommodation == "office");
    }

    return vacancies.filter(vacancies => vacancies.accommodation == "remote");

  }

}
