import { Pipe, PipeTransform } from '@angular/core';
import {Vacancy} from '../../models/vacancy.model';

@Pipe({
  name: 'salary'
})
export class SalaryPipe implements PipeTransform {

  transform(vacancies: Vacancy[], filter:string): Vacancy[] {

    const filterNumber: number = parseInt(filter);

    if (filter == "any") {
      return vacancies;
    }
    else if (filter == "with salary") {
      return vacancies.filter(vacancies => vacancies.salary != null);
    }

    return vacancies.filter(vacancies => vacancies.salary != null && vacancies.salary >= filterNumber);

  }
}
