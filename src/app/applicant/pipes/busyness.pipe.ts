import { Pipe, PipeTransform } from '@angular/core';
import {Vacancy} from '../../models/vacancy.model';


@Pipe({
  name: 'busyness'
})
export class BusynessPipe implements PipeTransform {

  transform(vacancies: Vacancy[], filter:string): Vacancy[] {

    if (filter == "any") {
      return vacancies;
    }
    else if (filter == "full-time"){
      return vacancies.filter(vacancies => vacancies.busyness == "full time");
    }

    return vacancies.filter(vacancies => vacancies.busyness == "part time");

  }

}
