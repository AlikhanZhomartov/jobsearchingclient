import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

const PASSWORD_API= 'http://localhost:8080/api/applicant/settings/password/';

@Injectable({
  providedIn: 'root'
})
export class PasswordService {

  constructor(private http: HttpClient) { }

  saveNewPassword(password: string, newPassword: string): Observable<any> {
    return this.http.post(PASSWORD_API + 'update', {
      password: password,
      newPassword: newPassword
    });
  }

}
