import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Resume} from '../../models/resume.model';
import {Observable} from 'rxjs';
import {TokenStorageService} from '../../shared-service/token-storage.service';

const RESUME_API = 'http://localhost:8080/api/applicant/resume/';

@Injectable({
  providedIn: 'root'
})
export class CreateResumeService {

  constructor(private http: HttpClient) { }


  getResume():Observable<Resume> {
    return this.http.get<Resume>(RESUME_API);
  }

  saveResume(resume: Resume): Observable<any> {
     return this.http.post(RESUME_API + 'create',  resume);
  }

}
