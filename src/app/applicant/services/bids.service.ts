import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {TokenStorageService} from '../../shared-service/token-storage.service';
import {Vacancy} from '../../models/vacancy.model';
import {Bids} from '../../models/bids.model';

const BIDS_API = 'http://localhost:8080/api/applicant/bids/';

@Injectable({
  providedIn: 'root'
})
export class BidsService {

  constructor(private http: HttpClient) { }


  getAllBids(): Observable<Bids[]> {
    return this.http.get<Bids[]>(BIDS_API);
  }

  getAllRecommendations(): Observable<Vacancy[]> {
    return this.http.get<Vacancy[]>(BIDS_API + 'recommendations');
  }

  deleteBid(id: number): Observable<any> {
    return this.http.post(BIDS_API + 'delete-bid', {bidId: id});
  }

  makeBid(id: number): Observable<any> {
    return this.http.post(BIDS_API + 'make-bid', {vacancyId: id});
  }

  saveInFeatured(id: number): Observable<any> {
    return this.http.post(BIDS_API + 'save-vacancy', {vacancyId: id});
  }
}
