import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../../models/user.models';

const NAME_SURNAME_API = 'http://localhost:8080/api/applicant/settings/name-surname/';

@Injectable({
  providedIn: 'root'
})
export class NameSurnameService {

  constructor(private http: HttpClient) { }

  getNameAndSurname():Observable<User> {
    return this.http.get<User>(NAME_SURNAME_API);
  }

  saveNameAndSurname(name: string, surname: string): Observable<any> {
    return this.http.post(NAME_SURNAME_API + 'save', {
      firstname: name,
      lastname: surname
    });
  }
}
