import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Vacancy} from '../../models/vacancy.model';

const FEATURED_API = 'http://localhost:8080/api/applicant/featured/';

@Injectable({
  providedIn: 'root'
})
export class FeaturedService {

  constructor(private http: HttpClient) { }

  getAllFeaturedVacancies(): Observable<Vacancy[]> {
    return this.http.get<Vacancy[]>(FEATURED_API);
  }

  makeBid(id: number): Observable<any> {
    return this.http.post(FEATURED_API + 'make-bid', {vacancyId: id});
  }

  deleteFromFeatured(id: number): Observable<any> {
    return this.http.post(FEATURED_API + 'delete-featured-vacancy', {vacancyId:id});
  }
}
