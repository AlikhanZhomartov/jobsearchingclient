import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Vacancy} from '../../models/vacancy.model';
import {TokenStorageService} from '../../shared-service/token-storage.service';

const VACANCIES_API= 'http://localhost:8080/api/applicant/';

@Injectable({
  providedIn: 'root'
})
export class VacanciesService {

  constructor(private http: HttpClient) { }


  getAllVacancies(): Observable<Vacancy[]> {
    return this.http.get<Vacancy[]>(VACANCIES_API + 'vacancies');
  }

  makeBids(id: number): Observable<any> {
    return this.http.post(VACANCIES_API + 'make-bid', {vacancyId: id});
  }

  saveVacancy(id: number): Observable<any> {
    return this.http.post(VACANCIES_API + 'sava-vacancy', {vacancyId: id});
  }
}
