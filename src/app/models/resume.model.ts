export interface Resume {
    name: string
    surname: string
    mobile: string
    city: string
    day: number
    month: string
    year: number
    gender: string
    jobTitle?: string
    salary?: number
    currency?: string
    aboutMe: string
    educationType: string
    institution?: string
    faculty?: string
    specialization?: string
    graduateYear?: number
}
