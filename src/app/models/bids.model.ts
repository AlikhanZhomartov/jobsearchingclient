export interface Bids {
    id: number
    jobTitle: string
    companyTitle: string
    date: string
}
