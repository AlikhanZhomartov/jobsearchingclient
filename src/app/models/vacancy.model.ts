export interface Vacancy{
    id: number
    jobTitle: string
    salary?: number
    companyTitle: string
    city?: string
    description: string
    date: string
    experience: boolean
    busyness: string
    accommodation: string
}
