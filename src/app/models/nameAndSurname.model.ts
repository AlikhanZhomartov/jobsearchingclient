export interface NameAndSurname {
    name: string
    surname: string
}
