import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {ApplicantLayoutComponent} from './applicant/shared/components/applicant-layout/applicant-layout.component';
import {FeaturedComponent} from './applicant/featured/featured.component';
import {CreateResumePageComponent} from './applicant/create-resume-page/create-resume-page.component';
import {BidsComponent} from './applicant/bids/bids.component';

import { AngularResizedEventModule } from 'angular-resize-event';
import { NameSurnameComponent } from './applicant/profile-page/name-surname/name-surname.component';
import { PasswordComponent } from './applicant/profile-page/password/password.component';
import { ProfilePageLayoutComponent } from './applicant/profile-page/profile-page-layout/profile-page-layout.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {VacanciesPageComponent} from './applicant/vacancies-page/vacancies-page.component';
import { SalaryPipe } from './applicant/pipes/salary.pipe';
import { ExperiencePipe } from './applicant/pipes/experience.pipe';
import { BusynessPipe } from './applicant/pipes/busyness.pipe';
import { AccommodationPipe } from './applicant/pipes/accommodation.pipe';
import { SelectedFilterPipe } from './applicant/pipes/selected-filter.pipe';
import { BasicLayoutComponent } from './basic/shared/component/basic-layout/basic-layout.component';
import { MainPageComponent } from './basic/main-page/main-page.component';
import { ApplicantLoginPageComponent } from './basic/applicant-login-page/applicant-login-page.component';
import { EmployeeLoginPageComponent } from './basic/employee-login-page/employee-login-page.component';
import { LoginPageComponent } from './basic/login-page/login-page.component';
import { EmployeeRegistrationComponent } from './basic/employee-registration/employee-registration.component';
import { ApplicantRegistrationComponent } from './basic/applicant-registration/applicant-registration.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {authInterceptorProviders} from './interceptor/auth-interceptor.service';
import {NotifierModule} from 'angular-notifier';



@NgModule({
  declarations: [
    AppComponent,
    ApplicantLayoutComponent,
    VacanciesPageComponent,
    FeaturedComponent,
    CreateResumePageComponent,
    BidsComponent,
    NameSurnameComponent,
    PasswordComponent,
    ProfilePageLayoutComponent,
    SalaryPipe,
    ExperiencePipe,
    BusynessPipe,
    AccommodationPipe,
    SelectedFilterPipe,
    BasicLayoutComponent,
    MainPageComponent,
    ApplicantLoginPageComponent,
    EmployeeLoginPageComponent,
    LoginPageComponent,
    EmployeeRegistrationComponent,
    ApplicantRegistrationComponent,

  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        AngularResizedEventModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        NotifierModule
    ],
  providers: [authInterceptorProviders],
  bootstrap: [AppComponent]
})
// @ts-ignore
export class AppModule {

}

// @ts-ignore
