import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ApplicantLayoutComponent} from './applicant/shared/components/applicant-layout/applicant-layout.component';
import {VacanciesPageComponent} from './applicant/vacancies-page/vacancies-page.component';
import {CreateResumePageComponent} from './applicant/create-resume-page/create-resume-page.component';
import {FeaturedComponent} from './applicant/featured/featured.component';
import {BidsComponent} from './applicant/bids/bids.component';
import {ProfilePageLayoutComponent} from './applicant/profile-page/profile-page-layout/profile-page-layout.component';
import {NameSurnameComponent} from './applicant/profile-page/name-surname/name-surname.component';
import {PasswordComponent} from './applicant/profile-page/password/password.component';
import {BasicLayoutComponent} from './basic/shared/component/basic-layout/basic-layout.component';
import {MainPageComponent} from './basic/main-page/main-page.component';
import {ApplicantLoginPageComponent} from './basic/applicant-login-page/applicant-login-page.component';
import {EmployeeLoginPageComponent} from './basic/employee-login-page/employee-login-page.component';
import {LoginPageComponent} from './basic/login-page/login-page.component';
import {EmployeeRegistrationComponent} from './basic/employee-registration/employee-registration.component';
import {ApplicantRegistrationComponent} from './basic/applicant-registration/applicant-registration.component';
import {ApplicantGuardService} from './applicant/helper/applicant-guard.service';

const routes: Routes = [
  {path: '', component: BasicLayoutComponent, children: [
          {path: '', redirectTo: 'vacancies', pathMatch: 'full'},
          {path: 'vacancies', component: MainPageComponent},
          {path: 'login', component: LoginPageComponent, children: [
                  {path: '', redirectTo: 'applicant-login', pathMatch: 'full'},
                  {path: 'applicant-login', component: ApplicantLoginPageComponent},
                  {path: 'employee-login', component: EmployeeLoginPageComponent}
              ]},
          {path: 'applicant-registration', component: ApplicantRegistrationComponent},
          {path: 'employee-registration', component: EmployeeRegistrationComponent}
      ]},
  {path: 'applicant', component: ApplicantLayoutComponent, children: [
      {path: '', redirectTo: 'vacancies', pathMatch: 'full'},
      {path: 'vacancies', component: VacanciesPageComponent, canActivate: [ApplicantGuardService]},
      {path: 'create-resume', component: CreateResumePageComponent, canActivate: [ApplicantGuardService]},
      {path: 'profile', component: ProfilePageLayoutComponent, children: [
              {path: '', redirectTo:'name-and-surname', pathMatch: 'full'},
              {path: 'name-and-surname', component: NameSurnameComponent, canActivate: [ApplicantGuardService]},
              {path: 'password', component: PasswordComponent, canActivate: [ApplicantGuardService]}
          ]
      },
      {path: 'featured', component: FeaturedComponent},
      {path: 'bids', component: BidsComponent}
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
