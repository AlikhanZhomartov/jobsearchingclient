import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthApplicantService} from '../services/auth-applicant.service';
import {Router} from '@angular/router';
import {TokenStorageService} from '../../shared-service/token-storage.service';
import {Subscription} from 'rxjs';
import {NotificationService} from '../../shared-service/notification.service';
import {HttpErrorResponse} from '@angular/common/http';
import {NotificationType} from '../../enum/notification-type.enum';

@Component({
  selector: 'app-applicant-registration',
  templateUrl: './applicant-registration.component.html',
  styleUrls: ['./applicant-registration.component.css']
})
export class ApplicantRegistrationComponent implements OnInit, OnDestroy {

  form: FormGroup;
  private subscription: Subscription[] = [];
  public loader: boolean = false;

  constructor(private authApplicantService: AuthApplicantService,
              private router: Router,
              private tokenService: TokenStorageService,
              private notification: NotificationService
  ) {
    if (this.tokenService.getUser()) {
      this.router.navigate(['/applicant', 'vacancies'])
    }
    this.form = this.createForm();
  }

  ngOnInit(): void {

  }

  private createForm(): FormGroup {
    return this.form = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      newPassword: new FormControl('', [Validators.required, Validators.minLength(6)]),
      repeatedPassword: new FormControl('', [Validators.required, Validators.minLength(6)]),
      firstname: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z ]*[А-Яа-яЁё ]*$')]),
      lastname: new FormControl('',[Validators.required, Validators.pattern('^[a-zA-Z ]*[А-Яа-яЁё ]*$')])
    });
  }

  submit() {
    if (!this.form.invalid) {
      this.loader = !this.loader;
      this.subscription.push(
      this.authApplicantService.registration(this.form.value.email, this.form.value.newPassword, this.form.value.firstname, this.form.value.lastname)
          .subscribe( () => {
                this.loader = !this.loader;
                this.form.reset();
          },
              (errorResponse: HttpErrorResponse) => {
                this.loader = !this.loader;
                console.log(errorResponse);
                this.notification.notify(NotificationType.ERROR, "an error occurred, please try again!");
                   this.form.reset();
              }));
    }
  }


  ngOnDestroy(): void {
    this.subscription.forEach(sub => sub.unsubscribe());
  }
}
