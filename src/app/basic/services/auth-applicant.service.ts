import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../../models/user.models';

const AUTH_APPLICANT_API = 'http://localhost:8080/api/auth/applicant/';
const AUTH_BASIC_API = 'http://localhost:8080/api/auth/';

@Injectable({
  providedIn: 'root'
})
export class AuthApplicantService {


  constructor(private http: HttpClient) { }

  public login(email: string, password: string): Observable<HttpResponse<User>> {
    return this.http.post<User>(AUTH_BASIC_API + 'login', {
      email: email,
      password: password,
    },{observe: 'response'});
  }

  public registration(email: string, password: string, firstname: string, lastname: string): Observable<User | HttpErrorResponse> {
    return this.http.post<User | HttpErrorResponse>(AUTH_APPLICANT_API + 'registration', {
      email: email,
      password: password,
      firstname: firstname,
      lastname: lastname
    });
  }
}
