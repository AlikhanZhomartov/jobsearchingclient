import { Component, OnInit } from '@angular/core';
import {ResizedEvent} from 'angular-resize-event';

@Component({
  selector: 'app-basic-layout',
  templateUrl: './basic-layout.component.html',
  styleUrls: ['./basic-layout.component.css']
})
export class BasicLayoutComponent implements OnInit {

  isShown: boolean;

  constructor() {
    this.isShown = false;
  }

  ngOnInit(): void {

  }

  toggleShow():void {
    this.isShown = !this.isShown;
  }

  onResized(event: ResizedEvent){
    if (event.newWidth >= 750) {
      this.isShown = !this.isShown;
    }
  }

}
