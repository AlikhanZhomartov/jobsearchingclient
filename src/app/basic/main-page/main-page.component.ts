import { Component, OnInit } from '@angular/core';
import {Vacancy} from '../../models/vacancy.model';
import {TokenStorageService} from '../../shared-service/token-storage.service';
import {Router} from '@angular/router';
import {RoleType} from '../../enum/role.enum';
import {User} from '../../models/user.models';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {

  salary: string = "any";
  experience: string = "any";
  busyness: string = "any";
  accommodation: string = "any";
  selectedFilter: string = "any";

  vacancies: Vacancy [] = [{
    id: 1,
    jobTitle: "Java developer",
    companyTitle: "Astana it university",
    city: "Nur-sultan",
    description: "Онлайн. Только входящий трафик, никаких холодных звонков. - обработка входящих заявок (основная часть в текстовом виде). - консультация клиентов. - ввод данных в...\n" +
        "опыт работы менеджером по продажам в онлайн (либо работы в call центрах, а те кто в сопроводительном письме укажет название...\n",
    date: "19 апреля",
    experience: true,
    busyness: "part time",
    accommodation: "office"
  },
    {
      id: 2,
      jobTitle: "Java-senior developer",
      salary: 240000,
      companyTitle: "Astana it university",
      city: "Nur-sultan",
      description: "Онлайн. Только входящий трафик, никаких холодных звонков. - обработка входящих заявок (основная часть в текстовом виде). - консультация клиентов. - ввод данных в...\n" +
          "опыт работы менеджером по продажам в онлайн (либо работы в call центрах, а те кто в сопроводительном письме укажет название...\n",
      date: "19 апреля",
      experience: false,
      busyness: "full time",
      accommodation: "remote"
    }];

  constructor(private tokenService: TokenStorageService,
              private router: Router,
              ) {
    const user: User = this.tokenService.getUser();
    if (user != null && user.role == RoleType.ROLE_USER) {
    this.router.navigate(['/applicant', 'vacancies'])
    }
    else if (user != null && this.tokenService.getUser().role == RoleType.ROLE_EMPLOYEE) {
      this.router.navigate(['/employee', 'vacancies'])
    }
    else if (user != null && this.tokenService.getUser().role == RoleType.ROLE_SUPER_ADMIN) {
      this.router.navigate(['/admin', 'main'])
    }
  }

  ngOnInit(): void {

  }

  discharge() {
    this.salary = "any";
    this.experience = "any";
    this.busyness = "any";
    this.accommodation = "any"
  }
}
