import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import {Router} from '@angular/router';
import {TokenStorageService} from '../../shared-service/token-storage.service';
import {NotificationService} from '../../shared-service/notification.service';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {HeaderType} from '../../enum/header-type.enum';
import {NotificationType} from '../../enum/notification-type.enum';
import {AuthEmployeeService} from '../services/auth-employee.service';
import {User} from '../../models/user.models';

@Component({
  selector: 'app-employee-login-page',
  templateUrl: './employee-login-page.component.html',
  styleUrls: ['./employee-login-page.component.css']
})
export class EmployeeLoginPageComponent implements OnInit, OnDestroy {

  form: FormGroup;
  private subscription: Subscription[] = [];
  public loader: boolean = false;

  constructor(private authEmployeeService: AuthEmployeeService,
              private router: Router,
              private tokenService: TokenStorageService,
              private notification: NotificationService
  ) {
    if (this.tokenService.getUser()) {
      this.router.navigate(['/employee', 'vacancies'])
    }
    this.form = this.createForm();
  }

  ngOnInit(): void {

  }

  private createForm(): FormGroup {
    return this.form = new FormGroup({
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }

  submit() {
    if (!this.form.invalid) {
      this.loader = !this.loader;
      this.subscription.push(
          this.authEmployeeService.login(this.form.value.email, this.form.value.password)
              .subscribe((response: HttpResponse<User>) => {
                this.loader = !this.loader;
                this.tokenService.saveToken(response.headers.get(HeaderType.JWT_TOKEN)!);
                this.tokenService.saveUser(response.body!);
                this.router.navigate(['/employee', 'vacancies']);
              },(errorResponse: HttpErrorResponse) => {
                this.loader = !this.loader;
                console.log(errorResponse);
                this.notification.notify(NotificationType.ERROR, "an error occurred, please try again!");
                this.form.reset();
              })
      );
    }
  }

  ngOnDestroy(): void {
    this.subscription.forEach(sub => sub.unsubscribe());
  }
}
