import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import {Router} from '@angular/router';
import {TokenStorageService} from '../../shared-service/token-storage.service';
import {NotificationService} from '../../shared-service/notification.service';
import {HttpErrorResponse} from '@angular/common/http';
import {NotificationType} from '../../enum/notification-type.enum';
import {AuthEmployeeService} from '../services/auth-employee.service';

@Component({
  selector: 'app-employee-registration',
  templateUrl: './employee-registration.component.html',
  styleUrls: ['./employee-registration.component.css']
})
export class EmployeeRegistrationComponent implements OnInit, OnDestroy {

  form: FormGroup;
  private subscription: Subscription[] = [];
  public loader: boolean = false;

  constructor(private authEmployeeService: AuthEmployeeService,
              private router: Router,
              private tokenService: TokenStorageService,
              private notification: NotificationService
  ) {
    if (this.tokenService.getUser()) {
      this.router.navigate(['/employee', 'vacancies'])
    }
    this.form = this.createForm();
  }

  ngOnInit(): void {

  }

  private createForm(): FormGroup {
    return this.form = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      newPassword: new FormControl('', [Validators.required, Validators.minLength(6)]),
      repeatedPassword: new FormControl('', [Validators.required, Validators.minLength(6)]),
      firstname: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z ]*[А-Яа-яЁё ]*$')]),
      lastname: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z ]*[А-Яа-яЁё ]*$')]),
      company: new FormControl('',Validators.required)
    });
  }

  submit() {
    if (!this.form.invalid) {
      this.loader = !this.loader;
      this.subscription.push(
          this.authEmployeeService.registration(this.form.value.email, this.form.value.password, this.form.value.firstname, this.form.value.lastname, this.form.value.company)
              .subscribe( () => {
                    this.loader = !this.loader;
                  },
                  (errorResponse: HttpErrorResponse) => {
                    this.loader = !this.loader;
                    console.log(errorResponse);
                    this.notification.notify(NotificationType.ERROR, "an error occurred, please try again!");
                    this.form.reset();
                  }));
    }
  }

  ngOnDestroy(): void{
    this.subscription.forEach(sub => sub.unsubscribe());
  }

}
