import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthApplicantService} from '../services/auth-applicant.service';
import {Router} from '@angular/router';
import {TokenStorageService} from '../../shared-service/token-storage.service';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {Subscription} from 'rxjs';
import {HeaderType} from '../../enum/header-type.enum';
import {NotificationService} from '../../shared-service/notification.service';
import {NotificationType} from '../../enum/notification-type.enum';
import {User} from '../../models/user.models';

@Component({
  selector: 'app-applicant-login-page',
  templateUrl: './applicant-login-page.component.html',
  styleUrls: ['./applicant-login-page.component.css']
})
export class ApplicantLoginPageComponent implements OnInit, OnDestroy {


  public form: FormGroup;
  private subscription: Subscription[] = [];
  public loader: boolean = false;

  constructor(private authApplicantService: AuthApplicantService,
              private router: Router,
              private tokenService: TokenStorageService,
              private notification: NotificationService
              ) {
    if (this.tokenService.getUser()) {
       this.router.navigate(['/applicant', 'vacancies'])
    }
    this.form = this.createForm();
  }

  ngOnInit(): void {

  }

  private createForm(): FormGroup {
    return this.form = new FormGroup({
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }

  submit() {
    if (!this.form.invalid) {
     this.loader = !this.loader;
     this.subscription.push(
     this.authApplicantService.login(this.form.value.email, this.form.value.password)
         .subscribe((response: HttpResponse<User>) => {
           this.loader = !this.loader;
           this.tokenService.saveToken(response.headers.get(HeaderType.JWT_TOKEN)!);
           this.tokenService.saveUser(response.body!);
           this.router.navigate(['/applicant', 'vacancies']);
      },(errorResponse: HttpErrorResponse) => {
              this.loader = !this.loader;
              console.log(errorResponse);
              this.notification.notify(NotificationType.ERROR, "an error occurred, please try again!");
              this.form.reset();
         })
      );
    }
  }

  ngOnDestroy(): void {
      this.subscription.forEach(sub => sub.unsubscribe());
  }
}
